Contents of this directory (/overrides/default) will be copied into the root Najdispoj folder on
container build and will override existing files.

Check out `bratislava` or `olomouc` directories for an example of what an
override can look like.