/**
 * Default config - do not modify
 * 
 * Put the modified file into /overrides/<override>/client/
 */

// Bratislava
export default {
    title: 'Najdispoj',
    ticket: false,
    api: {
        host: 'http://localhost:8081/api'
    },
    locales: {
        default: 'sk',
        fallback: 'en',
        supported: ['en', 'sk']
    },
    map: {
        vehicles: true,
        vehiclesUrl: '',
        center: {
            lat: 48.759,
            lng: 19.7
        },
        zoom: 8,
    },
    sources: [
        {
            "name": "Dopravný podnik Bratislava, a.s.",
            "url": "https://dpb.sk/"
        },
        {
            "name": "Bratislavská integrovaná doprava, a.s.",
            "url": "https://www.bid.sk/"
        },
        {
            "name": "Dopravní podnik města Olomouce, a.s.",
            "url": "https://www.dpmo.cz/"
        },
        {
            "name": "Železnice Slovenskej republiky",
            "url": "https://www.zsr.sk/"
        },
        {
            "name": "KORDIS JMK, a.s.",
            "url": "https://www.idsjmk.cz/"
        },
        {
            "name": "Leo Express, s.r.o.",
            "url": "https://www.leoexpress.com/"
        },
        {
            "name": "OpenStreetMap contributors",
            "url": "https://www.openstreetmap.org/"
        },
    ],
}