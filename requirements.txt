paramiko == 2.11.0
types-paramiko == 2.11.6
requests == 2.28.1
types-requests == 2.28.11
fastapi == 0.73.0
uvicorn[standard] == 0.23.2
pytest
mypy
docker-compose
black
beautifulsoup4 == 4.11.1
apscheduler == 3.9.1.post1
pandas
gtfs-kit == 5.2.3
ipykernel == 6.19.4
pyproj == 3.4.1
pydrive
# Xapian has to be installed manually when used
