import logging
import urllib.request
import urllib.error
import requests
from bs4 import BeautifulSoup

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.core.gtfsfolder import GTFSFolder


class DPMOStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data from DPMO (Dopravní podnik města Olomouce, a.s.)
    """

    def __init__(self, path):
        self.folder = GTFSFolder(path).set_label("DPMO")

    def download_data(self, path: str) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        page_url = "https://www.dpmo.cz/informace-pro-cestujici/jizdni-rady/jizdni-rady-gtfs/"

        html_raw = requests.get(page_url).content
        parser = BeautifulSoup(html_raw, "html.parser")

        a_element = parser.select_one(".hlavni-obsah .dokumenty .nazev a")

        a_href = a_element.attrs["href"].rstrip("?")
        gtfs_url = f"https://www.dpmo.cz{a_href}"

        logging.info(f"🚉DPMO Downloading GTFS data from {gtfs_url}...")
        try:
            zip_path, _ = urllib.request.urlretrieve(gtfs_url)
        except urllib.error.URLError:
            logging.info("Download has failed.")
            return None

        logging.info("Download successful.")

        return GTFSFolder(path).set_label("DPMO").load_zip(zip_path)

    def load_data(self) -> GTFSFolder | None:
        gtfs_folder = self.download_data(self.folder.path)
        # return gtfs_folder.change_route_types("11", "800")
        return gtfs_folder.repair_feed_info("https://dpmo.cz/")
