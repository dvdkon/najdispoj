import logging
import os
import urllib.request
import urllib.error
import requests
from io import StringIO
from bs4 import BeautifulSoup

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.core.gtfsfolder import GTFSFolder


class IDSJMKStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data of IDS JMK (Integrovaný dopravní systém Jihomoravského kraje)
    """

    def __init__(self, path):
        self.folder = GTFSFolder(path).set_label("IDSJMK")

    def download_data(self, path: str) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        gtfs_url = f"https://www.arcgis.com/sharing/rest/content/items/379d2e9a7907460c8ca7fda1f3e84328/data"
        zip_path = f"{path}gtfs.zip"

        logging.info(f"🚉IDSJMK Downloading GTFS data from {gtfs_url}...")
        try:
            os.makedirs(os.path.dirname(zip_path), exist_ok=True)
            with open(zip_path, "wb+") as file:
                # urllib.urlretrieve didn't work (due to SSL verification)
                response = requests.get(gtfs_url)
                file.write(response.content)
        except requests.exceptions.RequestException as e:
            logging.info(f"❌ Error: {e}")
            logging.info("❌ Download has failed.")
            return None

        logging.info("✅ Done.")

        return GTFSFolder(path).set_label("IDSJMK").load_zip(zip_path)

    def load_data(self) -> GTFSFolder | None:
        gtfs_folder = self.download_data(self.folder.path)
        return gtfs_folder
