import os
import paramiko
import csv
import logging
from datetime import datetime
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from pydrive.files import GoogleDriveFile
from oauth2client.service_account import ServiceAccountCredentials

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.core.gtfsfolder import GTFSFolder
from server.credentials import gserviceaccount_json


class AMSStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data from AMS (Arriva Mobility Solutions), provided via a Google Drive folder.

    For Google Drive authentication, see https://pythonhosted.org/PyDrive/quickstart.html#authentication
    """

    # TODO turn this into a generic Google Drive provider?

    def __init__(self, path):
        self.folder = GTFSFolder(path).set_label("AMS")

    def download_data(self, path: str) -> GTFSFolder | None:
        """Download GTFS data to a given path."""
        folder_id = "1n9r_hGe-msl3bGa0q9vqI_ENODHT6n7x"

        def get_date(folder):
            """Helper function for getting dates from GoogleDriveFile's title"""
            folder = int(folder["title"].split("-")[0])
            return folder

        logging.info(f"🚉 Downloading AMS data to {path}...")
        os.makedirs(path, exist_ok=True)

        # Authenticate and create the PyDrive client
        gauth = GoogleAuth()
        gauth.credentials = ServiceAccountCredentials.from_json_keyfile_dict(
            gserviceaccount_json,
            "https://www.googleapis.com/auth/drive",
        )
        gauth.Authorize()
        drive = GoogleDrive(gauth)

        # Search for the file in the folder
        files = drive.ListFile(
            {"q": f"'{folder_id}' in parents and trashed=false"}
        ).GetList()

        # Get the file names
        dates = list(map(get_date, files))
        dates = sorted(dates)

        today = int(datetime.today().strftime("%Y%m%d"))

        # Find the most recent file that is not in the future
        latest_valid_date = None
        for date in dates:
            if date > today:
                break
            latest_valid_date = date

        file_name = f"{latest_valid_date}-AMS-gtfs.zip"

        file: GoogleDriveFile = list(
            filter(lambda f: f["title"] == file_name, files)
        )[0]

        # Download the zip file
        zip_path = path + file_name
        file.GetContentFile(zip_path)

        logging.info("✅ Successfully downloaded GTFS data.")

        return (
            GTFSFolder(path)
            .set_label("AMS")
            .load_zip(zip_path)
            .create_feed_info("https://arriva.sk/bratislava/")
        )

    def load_data(self) -> GTFSFolder | None:
        return self.download_data(self.folder.path)
