import os
import paramiko
import csv
import logging
from datetime import datetime

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.core.gtfsfolder import GTFSFolder


class DPBStaticDataProvider(IStaticDataProvider):
    """Provider of raw GTFS data from Dopravný podnik Bratislava."""

    def __init__(self, path, host, username, password):
        self.folder = GTFSFolder(path).set_label("DPB")
        self.host = host
        self.username = username
        self.password = password

    def get_folder(self) -> GTFSFolder:
        return self.folder

    def download_data(self, path: str) -> GTFSFolder | None:
        """Download GTFS data to a given path."""

        def get_date(folder):
            """Helper function for parsing folder names into dates."""
            folder = folder.split("_")[1]
            return folder

        logging.info(f"🚉 Downloading DPB data to {path}...")

        os.makedirs(path, exist_ok=True)

        ssh = paramiko.SSHClient()

        # Add key
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # allow_agent must be False, Would throw an SSHException otherwise:
        # https://stackoverflow.com/questions/63821224/python-paramiko-ssh-exception-sshexception-no-existing-session
        ssh.connect(
            self.host,
            username=self.username,
            password=self.password,
            allow_agent=False,
        )

        ftp = ssh.open_sftp()

        files = ftp.listdir()
        gtfs_folders = list(filter(lambda f: f.startswith("GTFS_"), files))
        gtfs_dates = list(map(get_date, gtfs_folders))
        # Sort from the most recent to oldest
        gtfs_sorted = sorted(gtfs_dates, reverse=True)

        # Open a SFTP client to read feed start/end dates
        sftp = ssh.open_sftp()

        for date in gtfs_sorted:
            gtfs_folder = "GTFS_" + date + "/"
            gtfs_files = ftp.listdir(gtfs_folder)

            # Check whether the feed is valid

            if "feed_info.txt" not in gtfs_files:
                logging.info(
                    "Feed %s does not contain feed_info.txt. Looking for an older feed...",
                    gtfs_folder,
                )
                continue

            with sftp.open(gtfs_folder + "feed_info.txt") as feed_info:
                line = feed_info.readlines()[1]
                info = list(csv.reader([line], delimiter=",", quotechar='"'))[0]

                feed_start_date = info[3]
                feed_end_date = info[4]
                today = datetime.today().strftime("%Y%m%d")

                if feed_start_date > today or today > feed_end_date:
                    logging.info(
                        "Feed %s is not valid today. Looking for an older feed...",
                        gtfs_folder,
                    )
                else:
                    logging.info("🚉DPB Downloading feed %s...", gtfs_folder)
                    for file in gtfs_files:
                        ftp.get(gtfs_folder + file, path + file)
                    break

        logging.info("✅ Successfully downloaded GTFS data.")

        return GTFSFolder(path).set_label("DPB")

    def load_data(self) -> GTFSFolder | None:
        return self.download_data(self.folder.path)
