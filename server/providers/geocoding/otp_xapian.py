from collections import defaultdict
from typing import Optional, TypedDict
import json
import logging
import math
import os
import unicodedata
import requests
import shutil
import statistics
import tempfile
import xapian

from server.core.coords import Coords
from server.core.mode import TransitMode
from server.core.place import Place, PlaceType
from server.interfaces.i_geocoding_provider import IGeocodingProvider

class OtpXapianGeocodingProvider(IGeocodingProvider):
    """
    Provider that takes stops from OTP and then creates a custom local
    full-text index.

    Requires OTP2 with GTFS GraphQL API enabled
    """

    class StopIndexData(TypedDict):
        id: str
        name: str
        lat: float
        lon: float
        modes: list[str]
        merged_ids: list[str]

    def __init__(self, otp_address: str):
        logging.info("Started making stop index...")
        stops = self._get_stops_with_modes(otp_address)
        self._process_stations(stops)
        self._process_common_names(stops)

        self.db_dir = tempfile.mkdtemp(prefix="najdispoj_xapian")
        self.db = xapian.WritableDatabase(
            os.path.join(self.db_dir, "xapian.db"))
        logging.debug(f"Created Xapian search DB in {self.db_dir}")

        tg = xapian.TermGenerator()
        for stop in stops.values():
            doc = xapian.Document()

            tg.set_document(doc)
            tg.index_text(" ".join(m.value for m in stop["modes"]), 1, "XMODE")
            tg.index_text(self._normalize(stop["name"]))

            data: OtpXapianGeocodingProvider.StopIndexData = {
                "id": stop["gtfsId"],
                "name": stop["name"],
                "lat": stop["lat"],
                "lon": stop["lon"],
                "modes": [m.value for m in stop["modes"]],
                "merged_ids": stop["merged_ids"]
                              if "merged_ids" in stops
                              else [stop["gtfsId"]]
            }

            doc.set_data(json.dumps(data))
            self.db.replace_document(stop["gtfsId"], doc)

    def __del__(self):
        self.db.close()
        shutil.rmtree(self.db_dir)

    def _get_stops_with_modes(self, otp_address: str) -> dict[str, dict]:
        otp_resp = requests.post(
            f"{otp_address}/otp/routers/default/index/graphql",
            headers={"Content-Type": "application/graphql"},
            data="""
            {
                stops {
                    gtfsId
                    name
                    lat
                    lon
                    parentStation {
                        gtfsId
                    }
                }
                stations {
                    gtfsId
                    name
                    lat
                    lon
                }
                routes {
                    mode
                    stops {
                        gtfsId
                    }
                }
            }
            """)
        otp_data = otp_resp.json()["data"]
        stops = {s["gtfsId"]: s for s in otp_data["stops"]} \
              | {s["gtfsId"]: s for s in otp_data["stations"]}
        for stop in stops.values():
            stop["modes"] = set()

        for route in otp_data["routes"]:
            try:
                mode = TransitMode(route["mode"])
            except ValueError:
                mode = TransitMode.OTHER
            for stop in route["stops"]:
                stops[stop["gtfsId"]]["modes"].add(mode)

        return stops

    def _process_stations(self, stops: dict[str, dict]):
        """
        Offer only full stations when stops are parts of one
        """
        to_delete = []
        for id, stop in stops.items():
            if "parentStation" not in stop or stop["parentStation"] is None:
                continue
            station = stops[stop["parentStation"]["gtfsId"]]
            station["modes"].update(stop["modes"])
            to_delete.append(id)

        for id in to_delete:
            del stops[id]

    def _process_common_names(self, stops: dict[str, dict]):
        """
        Merge nearby stops of the same name and mode, since users don't have
        any way to distinguish them from the search results
        """

        # Dead simple threshold of "euclidean distance" based on lat/lon
        # Very incorrect and will break near poles, but works for ČR/SR
        threshold = 0.005

        by_name = defaultdict(list)
        for stop in stops.values():
            by_name[stop["name"]].append(stop)

        i = 1
        for name, stops_with_name in by_name.items():
            grouped_ids = set()
            for stop in stops_with_name:
                if stop["gtfsId"] in grouped_ids: continue

                group = []
                for stop2 in stops_with_name:
                    if stop2["gtfsId"] in grouped_ids: continue
                    dist = math.sqrt((stop2["lat"] - stop["lat"])**2
                                     + (stop2["lon"] - stop["lon"])**2)
                    if dist < threshold \
                        and len(stop["modes"] & stop2["modes"]) > 0:
                        group.append(stop2)

                if len(group) > 1:
                    grouped_ids.update(s["gtfsId"] for s in group)
                    id = f"grouped-{i}"
                    stops[id] = {
                        "gtfsId": id,
                        "name": name,
                        "lat": statistics.mean(s["lat"] for s in group),
                        "lon": statistics.mean(s["lon"] for s in group),
                        "parentStation": None,
                        "modes": set.union(*(s["modes"] for s in group)),
                    }
                    i += 1
                    for stop2 in group:
                        del stops[stop2["gtfsId"]]

    def _normalize(self, name: str) -> str:
        """Lowercase and strip diacritics"""
        name = name.lower()
        return "".join(c for c in unicodedata.normalize("NFD", name)
                       if unicodedata.combining(c) != 230)

    def autocomplete(self, query: str) -> list[Place]:
        query = self._normalize(query)

        qp = xapian.QueryParser()
        qp.set_database(self.db)
        qp.add_boolean_prefix("mode", "XMODE")
        query = qp.parse_query(query, xapian.QueryParser.FLAG_PARTIAL)
        enquire = xapian.Enquire(self.db)
        enquire.set_query(query)

        places = []
        for match in enquire.get_mset(0, 10):
            # Don't return irrelevant results
            if match.percent < 50: break

            data: OtpXapianGeocodingProvider.StopIndexData = \
                json.loads(match.document.get_data().decode())
            places.append(Place(
                name={"primary": data["name"], "secondary": ""},
                coords=Coords(data["lat"], data["lon"]),
                type_=PlaceType.STOP,
                modes=[TransitMode(m) for m in data["modes"]],
                stop_ids=data["merged_ids"],
            ))

        return places

