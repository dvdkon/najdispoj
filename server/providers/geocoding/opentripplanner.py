import requests
import json

from server.interfaces.i_geocoding_provider import IGeocodingProvider
from server.core.place import Place, PlaceType, PlaceName
from server.core.coords import Coords

from server.utils.geojson import GeoJSON
from server.utils.configtools import ConfigTools


class OpenTripPlannerGeocodingProvider(IGeocodingProvider):
    """
    Provider of geocoding data from OpenTripPlanner 1.5.0.
    Doesn't support reverse geocoding.
    """

    def __init__(self, address, region: str = "default"):
        # IP address and port of an OpenTripPlanner 1.5.0 instance
        # (Without the trailing '/')
        self.address = address
        self.region = region

    def autocomplete(self, query: str) -> list[Place]:
        params: dict = {
            "query": query,
            "stops": True,
            "corners": False,
            "clusters": True,
            "autocomplete": True,
        }

        data = requests.get(
            f"{self.address}/otp/routers/{self.region}/geocode?",
            params=params,
        ).json()
        places: list[Place] = []

        # Used stop descriptions in unchanged format
        used_stop_descriptions = []

        # print("Data:", json.dumps(data, indent=2))
        for place in data:
            description = place["description"]
            # Remove the first word (which is always "stop" in v1)
            if description.startswith("stop "):
                description = description[5:]
            name: PlaceName = {
                "primary": description,
                "secondary": "Zastávka",
            }
            coords = Coords(
                place["lat"],
                place["lng"],
            )

            # Only add stops, the names of which haven't been used yet
            # This filters out multiple platforms of a single stop
            # TODO come up with a better solution
            if place["description"] not in used_stop_descriptions:
                used_stop_descriptions.append(place["description"])
                places.append(Place(name, coords, PlaceType.STOP))

        return places[:3]
