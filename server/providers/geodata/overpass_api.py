import urllib.request
import urllib.error
import urllib.parse
import os
import logging

from server.interfaces.i_geodata_provider import IGeodataProvider
from server.utils.configtools import ConfigTools
from server.core.osmfile import OSMFile
from server.core.coords import Coords
from server.core.bounding_box import BoundingBox


class OverpassAPIProvider(IGeodataProvider):
    """Provider of raw OSM data from Overpass API."""

    def __init__(self, address):
        # IP address and port of an Overpass API instance.
        # (Without the trailing '/')
        self.address = address

    def load_data(self, path, bbox: BoundingBox) -> OSMFile | None:
        os.makedirs(os.path.dirname(path), exist_ok=True)

        url = self.address + "/map?bbox=" + str(bbox)
        try:
            logging.info(
                f"🗺️  Downloading raw OSM data from {url} to {path}..."
            )
            urllib.request.urlretrieve(url, path)
        except urllib.error.URLError:
            logging.error("❌ Download has failed.")
            return None

        logging.info("✅ Done.")
        return OSMFile(path)


# NEFUNGUJE cd /workspace ; /usr/bin/env /usr/local/bin/python /root/.vscode-server/extensions/ms-python.python-2022.18.2/pythonFiles/lib/python/debugpy/adapter/../../debugpy/launcher 35965 -- -m uvicorn server.main:app
# FUNGUJE   cd /workspace ; /usr/bin/env /usr/local/bin/python /root/.vscode-server/extensions/ms-python.python-2022.18.2/pythonFiles/lib/python/debugpy/adapter/../../debugpy/launcher 41149 -- -m uvicorn server.main:app
