import urllib.request
import urllib.error
import urllib.parse
import os
import logging

from server.interfaces.i_geodata_provider import IGeodataProvider
from server.utils.configtools import ConfigTools
from server.core.osmfile import OSMFile
from server.core.coords import Coords
from server.core.bounding_box import BoundingBox


class GeofabrikProvider(IGeodataProvider):
    """Provider of raw OSM data from Geofabrik."""

    def load_data(self, path: str, url: str) -> OSMFile | None:
        # URL to a Geofabrik OSM file.
        os.makedirs(os.path.dirname(path), exist_ok=True)

        try:
            logging.info(
                f"🗺️  Downloading raw OSM data from {url} to {path}..."
            )
            urllib.request.urlretrieve(url, path)
        except urllib.error.URLError:
            logging.error("❌ Download has failed.")
            return None

        logging.info("✅ Done.")
        return OSMFile(path)
