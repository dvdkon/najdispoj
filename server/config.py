"""
Default config - do not modify

Put the modified file into /overrides/<override>/server/

Provider-independent variables are set in this file.
Provider-dependent variables should be set in the provider's
constructor (in main.py).

This file should mostly contain information about the deployment environment.
"""

network = {
    "host": "localhost",
    "port": 8081,  # 443 for https, 80 for http
    # Nginx -> Najdispoj Server - also redefine in docker-compose.yml
    "najdispoj_port": 25516,
    "dynamic_data_port": 50801,
}

area = {
    "bbox": (
        {"lat": 47.727, "lng": 15.524},
        {"lat": 49.635, "lng": 22.566},
    ),
    "center": {"lat": 48.759, "lng": 19.451},
}

debug_mode = False
pfaedle_path = "/usr/src/app/vendor/pfaedle/build/pfaedle"
