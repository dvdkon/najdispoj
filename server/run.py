import os
import sys
import logging

import config

if __name__ == "__main__":
    if "-dev" in sys.argv:
        logging.info("Running Najdispoj in DEVELOPMENT mode.")
        # Development mode
        os.system(
            "uvicorn server.main:app"
            " --proxy-headers"  # Allows the reverse proxy to handle TLS
            " --reload"
            " --reload-dir ./server"  # Don't reload on change outside ./server
            " --host 0.0.0.0"
            " --port " + str(config.network["najdispoj_port"])
        )
    else:
        logging.info("Running Najdispoj in PRODUCTION mode.")
        # Production mode
        os.system(
            "uvicorn server.main:app"
            " --proxy-headers"  # Allows the reverse proxy to handle TLS
            " --host 0.0.0.0"
            " --port " + str(config.network["najdispoj_port"])
        )
