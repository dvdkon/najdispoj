from server.core.plan import Plan


class IRoutingProvider:
    """
    An informal interface for routing providers - not all functions have to be
    overridden by a provider class.
    """

    def get_plan(self, params: dict) -> Plan:
        raise NotImplementedError()
