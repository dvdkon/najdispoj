from server.core.gtfsfolder import GTFSFolder


class IStaticDataProvider:
    """
    An informal interface for static data providers - not all functions have to be
    overridden by a provider class.
    """

    folder: GTFSFolder

    def load_data(self) -> GTFSFolder | None:
        raise NotImplementedError()

    def get_folder(self) -> GTFSFolder:
        raise NotImplementedError()
