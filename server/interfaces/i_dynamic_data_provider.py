from server.core.vehicle import Vehicle


class IDynamicDataProvider:
    """
    An informal interface for dynamic data providers - not all functions have to be
    overridden by a provider class.
    """

    def get_vehicles(self) -> dict[int, Vehicle]:
        raise NotImplementedError()
