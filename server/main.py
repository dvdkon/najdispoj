from server.init import (
    geodata_service,
    geocoding_service,
    static_data_service,
    routing_service,
    dynamic_data_service,
    app,
)


from apscheduler.schedulers.background import BackgroundScheduler

from server.providers.geodata.overpass_api import OverpassAPIProvider
from server.providers.geodata.geofabrik import GeofabrikProvider

from server.providers.routing.opentripplanner import (
    OpenTripPlannerRoutingProvider,
)

from server.providers.geocoding.photon import PhotonProvider
from server.providers.geocoding.opentripplanner import (
    OpenTripPlannerGeocodingProvider,
)
from server.providers.agencies.dpmo.static_data import DPMOStaticDataProvider
from server.providers.agencies.dpb.static_data import DPBStaticDataProvider
from server.providers.agencies.dpb.dynamic_data import DPBDynamicDataProvider
from server.providers.agencies.ams.static_data import AMSStaticDataProvider
from server.providers.agencies.zsr.static_data import ZSRStaticDataProvider
from server.providers.agencies.idsjmk.static_data import (
    IDSJMKStaticDataProvider,
)
from server.providers.agencies.leoexpress.static_data import (
    LeoExpressStaticDataProvider,
)

from server.core.osmfile import OSMFile

from containers.nginx.manager import NginxManager
from containers.opentripplanner1_5_0.manager import OpenTripPlanner1_5_0Manager

import logging
import server.config as config

from server.credentials import dpb_host, dpb_login, dpb_pass

logging.root.handlers = (
    []
)  # Clear logging handlers - without this line the basicConfig would sometimes be ignored
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

geodata_service.register_provider(
    "OverpassAPI", OverpassAPIProvider("https://overpass-api.de/api")
)
geodata_service.register_provider("Geofabrik", GeofabrikProvider())

geocoding_service.register_provider(
    "Photon", PhotonProvider("https://photon.komoot.io")
)
geocoding_service.register_provider(
    "OpenTripPlanner 1.5.0",
    OpenTripPlannerGeocodingProvider("http://opentripplanner150:25515"),
)

static_data_service.register_provider(
    "DPMO", DPMOStaticDataProvider("/mnt/data/gtfs/dpmo/")
)
static_data_service.register_provider(
    "ZSR", ZSRStaticDataProvider("/mnt/data/gtfs/zsr/")
)
static_data_service.register_provider(
    "DPB",
    DPBStaticDataProvider("/mnt/data/gtfs/dpb/", dpb_host, dpb_login, dpb_pass),
)
static_data_service.register_provider(
    "IDSJMK", IDSJMKStaticDataProvider("/mnt/data/gtfs/idsjmk/")
)
static_data_service.register_provider(
    "LeoExpress", LeoExpressStaticDataProvider("/mnt/data/gtfs/leoexpress/")
)
static_data_service.register_provider(
    "AMS", AMSStaticDataProvider("/mnt/data/gtfs/ams/")
)

dpmo_static_data = static_data_service.get_provider("DPMO").folder
dpb_static_data = static_data_service.get_provider("DPB").folder
ams_static_data = static_data_service.get_provider("AMS").folder
zsr_static_data = static_data_service.get_provider("ZSR").folder
idsjmk_static_data = static_data_service.get_provider("IDSJMK").folder
leoexpress_static_data = static_data_service.get_provider("LeoExpress").folder

otp_manager = OpenTripPlanner1_5_0Manager("/mnt/data/otp150", 25515, xmx="16G")

routing_service.register_provider(
    "OpenTripPlanner 1.5.0",
    OpenTripPlannerRoutingProvider("http://opentripplanner150:25515"),
)

dynamic_data_service.register_provider(
    "DPB",
    DPBDynamicDataProvider(
        "0.0.0.0", config.network["dynamic_data_port"], dpb_static_data
    ),
)

"""Nginx setup"""
nginx_manager = NginxManager(
    config.network["host"],
    config.network["port"],
    config.network["najdispoj_port"],
    dev_server=True,
    dynamic_data_port=config.network["dynamic_data_port"],
)


def update_osm():
    slovakia_pbf = OSMFile("/mnt/data/osm/slovakia.pbf")
    czech_republic_pbf = OSMFile("/mnt/data/osm/czech_republic.pbf")
    osm_files = []

    # Slovakia
    slovakia_pbf = geodata_service.load_data(
        "Geofabrik",
        slovakia_pbf.path,
        url="https://download.geofabrik.de/europe/slovakia-latest.osm.pbf",
    )
    if not slovakia_pbf:
        logging.error("Failed to download OSM data for Slovakia.")
        return
    osm_files.append(slovakia_pbf)

    # Czech Republic
    czech_republic_pbf = geodata_service.load_data(
        "Geofabrik",
        czech_republic_pbf.path,
        url="https://download.geofabrik.de/europe/czech-republic-latest.osm.pbf",
    )
    if not czech_republic_pbf:
        logging.error("Failed to download OSM data for Czech Republic.")
        return
    osm_files.append(czech_republic_pbf)

    # Merge OSM files
    OSMFile.merge(osm_files, merged_osm.path)

    # Filter merged OSM data
    merged_osm.filter(merged_filtered_osm)
    merged_filtered_osm.cat(merged_filtered_pbf.path)


def update_gtfs():
    gtfs_folders = []

    # DPMO (Olomouc public transport)
    loaded = static_data_service.load_data("DPMO")
    if not loaded:
        logging.error("Failed to download GTFS data.")
        return
    dpmo_static_data.generate_shapes(merged_filtered_osm)
    gtfs_folders.append(dpmo_static_data)

    # DPB (Bratislava public transport)
    loaded = static_data_service.load_data("DPB")
    if not loaded:
        logging.error("Failed to download GTFS data.")
        return
    (
        dpb_static_data.change_route_types("11", "3")
        .generate_shapes(merged_filtered_osm)
        .revert_routes()
        .change_route_types("11", "800")
        .replace_agency_id("01", "DPB")
    )
    gtfs_folders.append(dpb_static_data)

    # AMS (Bratislava regional buses)
    loaded = static_data_service.load_data("AMS")
    if not loaded:
        logging.error("Failed to download GTFS data.")
        return

    ams_static_data.generate_shapes(merged_filtered_osm)
    gtfs_folders.append(ams_static_data)

    # ŽSR
    loaded = static_data_service.load_data("ZSR")
    if not loaded:
        logging.error("Failed to download GTFS data.")
        return
    zsr_static_data.generate_shapes(merged_filtered_osm)
    gtfs_folders.append(zsr_static_data)

    # IDSJMK
    loaded = static_data_service.load_data("IDSJMK")
    if not loaded:
        logging.error("Failed to download GTFS data.")
        return
    (
        idsjmk_static_data.change_route_types("11", "3")
        .generate_shapes(merged_filtered_osm)
        .revert_routes()
        .change_route_types("11", "800")
    )
    gtfs_folders.append(idsjmk_static_data)

    # LeoExpress
    loaded = static_data_service.load_data("LeoExpress")
    if not loaded:
        logging.error("Failed to download GTFS data.")
        return
    (
        leoexpress_static_data.change_route_types("11", "3")
        .generate_shapes(merged_filtered_osm)
        .revert_routes()
        .change_route_types("11", "800")
    )
    gtfs_folders.append(leoexpress_static_data)

    # Link OSM and build graph
    otp_manager.link_osm(merged_filtered_pbf.path)
    otp_manager.build_graph(gtfs_folders)


# Config variables
merged_osm = OSMFile("/mnt/data/osm/merged.osm")
merged_filtered_osm = OSMFile("/mnt/data/osm/merged_filtered.osm")
merged_filtered_pbf = OSMFile("/mnt/data/osm/merged_filtered.pbf")


def cronjob():
    update_gtfs()
    otp_manager.serve(restart=True)


@app.on_event("startup")
def init_app():
    # Add scheduled jobs
    scheduler = BackgroundScheduler()
    scheduler.add_job(cronjob, "cron", hour="4")
    scheduler.start()

    nginx_manager.build_client()
    otp_manager.serve(restart=True)
