import math
from server.core.leg import Leg
from server.core.mode import TransitMode


class Itinerary:
    """Represents an itinerary in a plan."""

    def __init__(self, duration: int, departure: int, arrival: int, legs: list[Leg]):
        self._duration = duration
        self._departure = departure
        self._arrival = arrival
        self._legs = legs
        self._timeline_duration: int | None = None

        self.compute_waiting()

    def compute_timeline(self, max_duration) -> None:
        """Compute timeline duration for all legs of the itinerary."""
        self._timeline_duration = round(self._duration / max_duration * 100, 2)

        for leg in self._legs:
            leg.compute_timeline_duration(max_duration)

    def compute_waiting(self) -> None:
        """Compute waiting duration for all legs of the itinerary."""
        leg_len = len(self._legs)

        for index, leg in enumerate(self._legs):
            if index < leg_len - 2 and index != 0:
                next_leg = self._legs[index + 1]
                if leg.get_mode() == TransitMode.WALK:
                    waiting_duration = math.floor(
                        (int(next_leg.get_departure()) - int(leg.get_arrival())) / 1000
                    )
                else:
                    waiting_duration = math.floor(
                        (
                            int(next_leg.get_departure())
                            - int(leg.get_stops()[-1].get_departure())
                        )
                        / 1000
                    )

                if (
                    waiting_duration > 60
                    and leg.get_mode() == TransitMode.WALK
                    and next_leg.get_mode() != TransitMode.WALK
                ):
                    waiting_duration -= 60

                leg.set_waiting_duration(waiting_duration)

    def get_duration(self) -> int:
        return self._duration

    def get_departure(self) -> int:
        return self._departure

    def get_arrival(self) -> int:
        return self._arrival

    def get_legs(self) -> list[Leg]:
        return self._legs

    def to_json(self) -> dict:
        """
        Returns an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        return {
            "duration": self._duration,
            "departure": self._departure,
            "arrival": self._arrival,
            "legs": list(map(lambda leg: leg.to_json(), self._legs)),
            "timelineDuration": self._timeline_duration,
        }
