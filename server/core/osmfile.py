import os
import logging
import shutil

from server.core.bounding_box import BoundingBox


class OSMFile:
    # TODO add set_color_by_route_type
    def __init__(self, path: str):
        """
        Manager of an OSM file.

        Provides basic tools for working with the file.

        Args:
            path (str): The path to an .osm or .pbf file.
        """
        self.path = path
        self.label = "default_label"

    def alter(self):
        """
        Create OSM file for OTP routing
        By replacing highway=residential by highway=cycleway we
        make sidewalks the (slightly more) preferred option for walking.
        TODO In the future, create a pull request to OTP repository and remove this
        workaround.
        """
        logging.info("🗺️  Altering OSM data...")

        os.system(
            f"osmfilter {self.path} --modify-tags='highway=residential to =cycleway' -o=temp.osm"
        )
        shutil.move("temp.osm", self.path)

        logging.info(f"✅ Done.")
        return self

    def crop(self, bbox: BoundingBox):
        # TODO is working?
        logging.info("🗺️  Cropping OSM data...")

        os.system(
            f"osmium extract --strategy complete_ways --bbox {bbox} {self.path} -o temp.osm --overwrite"
        )
        shutil.move("temp.osm", self.path)

        logging.info(f"✅ Done.")
        return self

    def borders(self, path):
        logging.info("🗺️  Getting borders of OSM data...")

        cmd = f"""osmium tags-filter {self.path} \
            w/boundary=administrative \
            -o /mnt/data/osm/temp.osm --overwrite"""

        logging.info(f"🏃 Running command {cmd}:")

        os.system(cmd)
        shutil.move("/mnt/data/osm/temp.osm", path)

        logging.info(f"✅ Done.")
        return OSMFile(path)

    def filter(self, osmfile=None):
        """Filters OSM data to keep only relevant tags for OTP routing."""
        logging.info("🗺️  Filtering OSM data...")

        output_path = osmfile.path if osmfile else self.path
        output_format = output_path.split(".")[-1]

        cmd = f"""osmium tags-filter {self.path} \
            w/highway \
            w/public_transport \
            w/railway \
            r/type=restriction \
            r/type=route \
            -o /mnt/data/osm/temp.osm -f {output_format},add_metadata=false"""

        logging.info(f"🏃 Running command {cmd}:")

        os.system(cmd)
        shutil.move("/mnt/data/osm/temp.osm", output_path)

        logging.info(f"✅ Done.")

        return osmfile if osmfile else self

    def cat(self, path: str):
        logging.info(
            f"🗺️  Concatenating OSM data from {self.path} to {path}..."
        )

        os.system(f"osmium cat {self.path} -o {path} --overwrite")

        logging.info("✅ Done.")
        return OSMFile(path)

    def set_label(self, label: str):
        self.label = label
        return self

    @staticmethod
    def merge(osm_files: list, destination):
        logging.info("🗺️  Merging OSM data...")

        paths = map(lambda f: f.path, osm_files)
        paths = " ".join(paths)
        os.system(f"osmium merge {paths} -o {destination} --overwrite")

        logging.info("✅ Done.")
        return OSMFile(destination)
