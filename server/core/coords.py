class Coords:
    """Represents a coordinate (latitude, longitude) pair."""

    def __init__(self, lat: float, lng: float):
        self._lat = lat
        self._lng = lng

    def get_lat(self) -> float:
        return self._lat

    def get_lng(self) -> float:
        return self._lng

    def to_json(self) -> dict:
        """
        Returns an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        return {"lat": self._lat, "lng": self._lng}

    def __str__(self) -> str:
        return str(self._lat) + "," + str(self._lng)

    @classmethod
    def from_json(cls, coords: dict):
        """Alternative constructor"""
        return cls(coords["lat"], coords["lng"])
