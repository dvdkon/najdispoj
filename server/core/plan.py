from server.core.itinerary import Itinerary
import server.config as config


class Plan:
    def __init__(self, itineraries=[]):
        self._itineraries: list[Itinerary] = itineraries
        self._raw: str = ""

    def add_itinerary(self, itinerary: Itinerary):  # TODO remove?
        self._itineraries.append(itinerary)

    def set_raw(self, raw: str):
        self._raw = raw

    def compute_timelines(self):
        max_duration = 0
        for itinerary in self._itineraries:  # TODO rewrite using reduce
            if itinerary.get_duration() > max_duration:
                max_duration = itinerary.get_duration()

        for itinerary in self._itineraries:
            itinerary.compute_timeline(max_duration)

    def to_json(self):
        """
        Returns an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        json = {
            "itineraries": list(
                map(lambda itinerary: itinerary.to_json(), self._itineraries)
            ),
        }

        if config.debug_mode:
            json["raw"] = self._raw

        return json
