import docker
from enum import Enum
import os
import logging
import re


class ContainerStatus(Enum):
    """
    All the standard Docker container statuses
    (created, restarting, running, removing, paused, exited, dead)
    + missing Dockerfile, missing image, missing container, unknown
    """

    MISSING_DOCKERFILE = 0
    MISSING_IMAGE = 1
    MISSING_CONTAINER = 2
    CREATED = 3
    RESTARTING = 4
    RUNNING = 5
    REMOVING = 6
    PAUSED = 7
    EXITED = 8
    DEAD = 9
    UNKNOWN = 10


class Container:
    """
    A class, which simplifies the management of a Docker container
    associated with a Najdispoj instance (e.g. a trip planning service).
    """

    # TODO add virtual terminal, the output of which can be displayed e.g. in
    # admin panel, or during initialization

    def __init__(
        self,
        client: docker.DockerClient,
        dockerfile_path: str,
        tag: str,
        name: str,
        network: str,
        ports: list | dict = [],
        volumes: list | dict = [],
        environment: list | dict = [],
        buildargs: dict = {},
        detach: bool = True,
        cap_add: list = [],
        restart_on_reload: bool = True,
    ):
        self.client = client
        self.dockerfile_path = dockerfile_path
        self.tag = tag
        self.name = name
        self.network = network
        self.ports = ports
        self.volumes = volumes
        self.environment = environment
        self.buildargs = buildargs
        self.detach = detach
        self.cap_add = cap_add
        self.restart_on_reload = restart_on_reload

        match self.get_status():
            case ContainerStatus.MISSING_DOCKERFILE:
                raise FileNotFoundError("Dockerfile not found.")
            case ContainerStatus.MISSING_IMAGE:
                # TODO check whether the container is missing too
                self.build_image()
                self.create_container()
                self.start_container()
            case ContainerStatus.MISSING_CONTAINER:
                self.create_container()
                self.start_container()
            case ContainerStatus.EXITED | ContainerStatus.CREATED:
                self.start_container()
            case ContainerStatus.PAUSED:
                # self.unpause_container()
                self.restart_container()
            case ContainerStatus.RUNNING:
                if self.restart_on_reload:
                    self.restart_container()
            case status:
                # TODO maybe raise a different type of exception
                logging.error(self.get_container())
                raise RuntimeError(
                    "Error: An unhandled container status was reached:", status
                )

    def get_status(self) -> ContainerStatus:
        """
        Return the current status of Docker container. Supports additional
        statuses, which are not a part of Docker.
        """

        # Check if Dockerfile exists
        if not os.path.exists(self.dockerfile_path):
            return ContainerStatus.MISSING_DOCKERFILE

        # Check if an image has already been built
        # TODO check whether the Dockerfile has been updated
        image_exists = False
        images = self.client.images.list()
        for img in images:
            if self.tag in img.tags:
                image_exists = True
                break
        if not image_exists:
            return ContainerStatus.MISSING_IMAGE

        # Check if a container has already been created
        container = self.get_container()
        if not container:
            return ContainerStatus.MISSING_CONTAINER

        # Get the status from the container, or return unknown
        match container.status:
            case "created":
                return ContainerStatus.CREATED
            case "restarting":
                return ContainerStatus.RESTARTING
            case "running":
                return ContainerStatus.RUNNING
            case "removing":
                return ContainerStatus.REMOVING
            case "paused":
                return ContainerStatus.PAUSED
            case "exited":
                return ContainerStatus.EXITED
            case "dead":
                return ContainerStatus.DEAD
            case _:
                return ContainerStatus.UNKNOWN

    def get_container(self) -> docker.models.containers.Container:
        """Return a Container object or False if not found."""

        containers = self.client.containers.list("all")
        for container in containers:
            if container.name == self.name:
                return container
        return False

    def build_image(self) -> None:
        """Build an image from the Dockerfile (docker build)."""

        logging.info("Building image...")

        self.client.images.build(
            path=self.dockerfile_path,
            nocache=True,
            pull=True,
            tag=self.tag,
            buildargs=self.buildargs,
            rm=True,  # Maybe forcerm will be necessary
        )

        logging.info("✅ Successfully built image.")

    def create_container(self, log_output: bool = False) -> None:
        """Create the Docker container (docker create)."""

        logging.info("Creating container...")

        output = self.client.containers.create(
            self.tag,
            name=self.name,
            hostname=self.name,
            tty=True,  # without this line, command execution wouldn't work
            volumes=self.volumes,
            ports=self.ports,
            detach=self.detach,
            network=self.network,
            environment=self.environment,
            cap_add=self.cap_add,
        )

        if log_output:
            output = output.decode("utf-8")
            print(output)

    def restart_container(self) -> None:
        """Restart the Docker container (docker restart)."""
        logging.info(f"📦{self.name} Restarting container...")
        self.get_container().restart()

    def start_container(self) -> None:
        """Start the Docker container (docker start)."""
        logging.info(f"📦{self.name} Starting container...")
        self.get_container().start()

    def unpause_container(self) -> None:
        """Unpause the docker container (docker unpause)."""
        logging.info(f"📦{self.name} Unpausing container...")
        self.get_container().unpause()

    def run_command(
        self,
        cmd: list | str,
        log_output: bool = False,
        wait_until: str = "",
        stream: bool = True,
        tty: bool = True,
        detach: bool = False,
    ) -> None:
        """
        Run a command inside the Docker container.

        Args:
            log_output (bool): If True, keep printing command output until the
                command finishes.
            wait_until (str): Wait until a string is found inside a line from
                output (using re.search()).
        """

        if isinstance(cmd, list):
            logging.info(f"📦{self.name} 🏃 Running command: {' '.join(cmd)}")
        else:
            logging.info(f"📦{self.name} 🏃 Running command: {cmd}")

        self.get_container().start()  # TODO try whether this is necessary
        result = self.get_container().exec_run(
            cmd, stream=stream, tty=tty, detach=detach
        )

        if log_output or wait_until != "":
            for line in result.output:
                lines_decoded = line.decode("utf-8").strip("\r\n")

                if log_output and lines_decoded:
                    for line in lines_decoded.split("\n"):
                        print(f"    📦{self.name}: {line}")

                if wait_until != "" and re.search(wait_until, lines_decoded):
                    break

        logging.info(f"📦{self.name} ✅ Done running command.")

    def connect_network(self, name: str) -> None:
        """Connect Docker container to a network (docker network connect)."""
        # TODO check if the container is already connected

        networks = self.client.networks.list(name)
        if networks == []:  # TODO if not networks?
            logging.error("Network %s does not exist.", name)
            return

        network = networks[0]
        network.connect(self.get_container())
