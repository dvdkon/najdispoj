from server.core.coords import Coords


class BoundingBox:
    """
    Represents a coordinate (latitude, longitude) pair.

    Args:
        coords_min (Coords): Coords with smaller longitude and latitude
        coords_max (Coords): Coords with bigger longitude and latitude
    """

    def __init__(self, coords_min: Coords, coords_max: Coords):
        self._coords_min = coords_min
        self._coords_max = coords_max

    def __str__(self) -> str:
        """
        Returns a string of the following format:

        coords_min.lng,coords_min.lat,coords_max.lng,coords_max.lat
        """
        return f"{self._coords_min.get_lng()},{self._coords_min.get_lat()},{self._coords_max.get_lng()},{self._coords_max.get_lat()}"

    def add_coords(self, coords: Coords) -> None:
        """
        Expands the bounding box to include the given Coords.

        Args:
            coords (Coords): Coords to add
        """
        self._coords_min = Coords(
            min(self._coords_min.get_lat(), coords.get_lat()),
        )
        self._coords_min.set_lng(
            min(self._coords_min.get_lng(), coords.get_lng())
        )
        self._coords_max.set_lat(
            max(self._coords_max.get_lat(), coords.get_lat())
        )
        self._coords_max.set_lng(
            max(self._coords_max.get_lng(), coords.get_lng())
        )
