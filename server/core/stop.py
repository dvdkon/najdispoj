from server.core.coords import Coords


class Stop:
    def __init__(self, name: str, coords: Coords, departure: int):
        self._name = name
        self._coords = coords
        self._departure = departure

    def get_name(self):
        return self._name

    def get_coords(self):
        return self._coords

    def get_departure(self):
        return self._departure

    def to_json(self):
        return {
            "name": self._name,
            "coords": self._coords.to_json(),
            "departure": self._departure,
        }
