import server.config as cfg

from server.core.coords import Coords
from server.core.bounding_box import BoundingBox


class ConfigTools:
    """
    Helper methods for creating objects from the config properties.
    """

    @staticmethod
    def get_bbox() -> BoundingBox:
        return BoundingBox(
            Coords.from_json(cfg.area["bbox"][0]),
            Coords.from_json(cfg.area["bbox"][1]),
        )

    @staticmethod
    def get_center() -> Coords:
        return Coords.from_json(cfg.area["center"])
