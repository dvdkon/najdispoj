from server.core.place import PlaceName, PlaceType


class GeoJSON:
    @staticmethod
    def get_name(obj: dict) -> PlaceName:
        name: PlaceName = {"primary": "", "secondary": ""}

        props = obj["properties"]

        if "name" in props:
            name["primary"] = props["name"]
        elif "street" in props and "housenumber" in props:
            name["primary"] = props["street"] + " " + props["housenumber"]
        else:
            name["primary"] = "Miesto bez názvu"

        if "city" in props:
            name["secondary"] = props["city"]
            if "locality" in props:
                name["secondary"] += f", {props['locality']}"

        return name

    @staticmethod
    def get_type(obj: dict) -> PlaceType:
        props = obj["properties"]

        if "osm_key" in props:
            if props["osm_key"] == "highway":
                return PlaceType.STREET

            if props["osm_key"] == "shop":
                return PlaceType.SHOP

            if props["osm_key"] == "amenity":
                return PlaceType.INSTITUTION

        if "osm_value" in props:
            if (
                props["osm_value"] == "bus_stop"
                or props["osm_value"] == "platform"
            ):
                return PlaceType.STOP

            if props["osm_value"] in [
                "town",
                "neighbourhood",
                "suburb",
                "village",
            ]:
                return PlaceType.TOWN

            if props["osm_value"] in ["residential"]:
                return PlaceType.BUILDING

            if props["osm_value"] in ["school"]:
                return PlaceType.INSTITUTION

        if "street" in props and "housenumber" in props:
            return PlaceType.BUILDING

        return PlaceType.OTHER
