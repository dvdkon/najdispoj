from fastapi import FastAPI

from server.services.geodata_service import GeodataService
from server.services.geocoding_service import GeocodingService
from server.services.routing_service import RoutingService
from server.services.static_data_service import StaticDataService
from server.services.dynamic_data_service import DynamicDataService
from server.services.api_service import APIService

geodata_service = GeodataService()
geocoding_service = GeocodingService()
static_data_service = StaticDataService(geodata_service)
routing_service = RoutingService(static_data_service, geodata_service)
dynamic_data_service = DynamicDataService()
api_service = APIService(
    geocoding_service, routing_service, dynamic_data_service
)

app = FastAPI()


@app.get("/api/reverse")
async def reverse(coords: str):
    return api_service.reverse_geocode(coords)


@app.get("/api/autocomplete")
async def autocomplete(query: str):
    return api_service.autocomplete(query)


@app.get("/api/plan")
async def plan(
    origin: str,
    destination: str,
    arriveBy: bool,
    date: str,
    time: str,
    maxTransfers: int,
    maxWalkDistance: int,
    walkSpeed: int,
    optimize: str,
):
    # TODO make parameters lowercase (must be changed on client too)
    return api_service.get_plan(
        origin,
        destination,
        arriveBy,
        date,
        time,
        maxTransfers,
        maxWalkDistance,
        walkSpeed,
        optimize,
    )


@app.get("/api/vehicles")
async def vehicles():
    return api_service.get_vehicles()
