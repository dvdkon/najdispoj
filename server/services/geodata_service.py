import logging

from server.core.osmfile import OSMFile
from server.interfaces.i_geodata_provider import IGeodataProvider

import server.config as cfg


class GeodataService:
    def __init__(self):
        self.providers: dict[str, IGeodataProvider] = {}

    def register_provider(self, name: str, provider: IGeodataProvider):
        """Registers and initializes a geodata provider."""
        logging.info("➕ Registered a geodata provider: %s", name)
        self.providers[name] = provider
        return self

    def load_data(self, provider, destination, **kwargs) -> OSMFile | None:
        """Downloads raw OSM data."""
        return self.providers[provider].load_data(destination, **kwargs)
