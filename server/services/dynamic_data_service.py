import logging
import time

from server.interfaces.i_dynamic_data_provider import IDynamicDataProvider
from server.core.vehicle import Vehicle


class DynamicDataService:
    def __init__(self):
        self.providers: dict[str, IDynamicDataProvider] = {}

    def register_provider(self, name: str, provider: IDynamicDataProvider):
        """Registers and initializes a dynamic data provider."""
        logging.info("➕ Registered a dynamic data provider: %s", name)
        self.providers[name] = provider
        return self

    def get_vehicles(self) -> dict[str, Vehicle]:
        def filter_func(v: tuple[str, Vehicle]) -> bool:
            displayable = bool(
                v[1].coords
                and (v[1].routeShortName or v[1].routeLongName)
                and v[1].lastUpdate > time.time() - 300
            )
            return displayable

        result = {}
        for provider in self.providers.keys():
            vehicles = self.providers[provider].get_vehicles()
            vehicles = dict(filter(filter_func, vehicles.items()))

            result.update(vehicles)

        return result
