import json
from server.services.dynamic_data_service import DynamicDataService

from server.services.geocoding_service import GeocodingService
from server.services.routing_service import RoutingService

from server.core.coords import Coords
from server.core.place import Place


class APIService:
    """
    Converts request attributes to Najdispoj's data types and calls functions
    of other services. Note: Methods of APIService should return an object,
    which can be losslessly converted into JSON.
    """

    def __init__(
        self,
        geocoding_service: GeocodingService,
        routing_service: RoutingService,
        dynamic_data_service: DynamicDataService | None = None,
    ):
        self.geocoding_service = geocoding_service
        self.routing_service = routing_service
        self.dynamic_data_service = dynamic_data_service

    def reverse_geocode(self, coords: str) -> dict:
        return self.geocoding_service.reverse_geocode(
            Coords.from_json(json.loads(coords))
        ).to_json()

    def autocomplete(self, query: str) -> dict:
        return {
            "query": query,
            "places": list(
                map(
                    lambda place: place.to_json(),
                    self.geocoding_service.autocomplete(query),
                )
            ),
        }

    def get_plan(
        self,
        origin: str,
        destination: str,
        arrive_by: bool,
        date: str,
        time: str,
        max_transfers: int,
        max_walk_distance: int,
        walk_speed: int,
        optimize: str,
    ):
        plan = self.routing_service.get_plan(
            {
                "origin": Coords.from_json(json.loads(origin)),
                "destination": Coords.from_json(json.loads(destination)),
                "arrive_by": arrive_by,
                "date": date,
                "time": time,
                "max_transfers": max_transfers,
                "max_walk_distance": max_walk_distance,
                "walk_speed": walk_speed,
                "optimize": optimize,
            }
        ).to_json()

        return {"plan": plan}

    def get_vehicles(self):
        if self.dynamic_data_service == None:
            return []
        vehicles = self.dynamic_data_service.get_vehicles()

        result = []
        for _, vehicle in vehicles.items():
            result.append(vehicle.to_json())

        return result
