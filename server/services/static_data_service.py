import logging

from server.interfaces.i_static_data_provider import IStaticDataProvider

import server.config as cfg
from server.core.gtfsfolder import GTFSFolder


class StaticDataService:
    """
    TODO differentiate between downloading and "applying" (setting as currently
    used) GTFS data. Add config parameter "preserve old datasets".
    """

    def __init__(self, geodata_service):
        self.geodata_service = geodata_service
        self.providers: dict[str, IStaticDataProvider] = {}

    def register_provider(self, name: str, provider: IStaticDataProvider):
        """Registers and initializes a static data provider."""
        logging.info("➕🚉 Registered a static data provider: %s", name)
        self.providers[name] = provider

    def get_provider(self, name: str) -> IStaticDataProvider:
        return self.providers[name]

    def load_data(self, provider: str) -> bool:
        """Downloads raw GTFS data to a folder."""
        return self.providers[provider].load_data() != None
