import logging
import collections

from server.core.plan import Plan
from server.interfaces.i_routing_provider import IRoutingProvider
from server.services.static_data_service import StaticDataService
from server.services.geodata_service import GeodataService


class RoutingService:
    def __init__(
        self,
        static_data_service: StaticDataService,
        geodata_service: GeodataService,
    ):
        self.static_data_service = static_data_service
        self.geodata_service = geodata_service
        self.providers: dict[str, IRoutingProvider] = collections.OrderedDict()

    def register_provider(self, name: str, provider: IRoutingProvider):
        """Registers and initializes a routing provider."""
        self.providers[name] = provider
        logging.info("New routing service: %s", name)
        return self

    def get_plan(self, params) -> Plan:
        for provider in self.providers.values():
            return provider.get_plan(params)
        raise NotImplementedError()
