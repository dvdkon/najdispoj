FROM python:3.10.2

WORKDIR /usr/src/app

RUN apt-get update

# Install Docker
RUN	apt-get install ca-certificates curl gnupg lsb-release -y &&\
	mkdir -p /etc/apt/keyrings &&\
	curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg &&\
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
	$(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null &&\
	chmod a+r /etc/apt/keyrings/docker.gpg &&\
	apt-get update &&\
	apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y

# Build & install Pfaedle
ARG PFAEDLE_PATH=/usr/src/app/vendor/pfaedle
RUN apt-get install cmake -y &&\
	git clone --recurse-submodules https://github.com/ad-freiburg/pfaedle ${PFAEDLE_PATH}
# WORKDIR ${PFAEDLE_PATH}
# RUN git checkout 167753142c7dd50e6446bb4a34290f971918e10a

WORKDIR ${PFAEDLE_PATH}/build

RUN cmake .. &&\
	make -j &&\
	make install

# Install OSM tools
RUN apt-get install osmctools -y &&\
	apt-get install osmium-tool -y

# Install Node.js
RUN apt-get install npm -y &&\
	npm install npm@6.14.11 -g &&\
	npm install n -g &&\
	n 14.16.0
RUN npm install @vue/cli -g

WORKDIR /usr/src/app

# Set up Python environment
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

ARG override=default
COPY /overrides/${override}/ .

CMD [ "python", "server/run.py" ]